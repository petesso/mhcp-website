<?php
/**
 * Grundeinstellungen für WordPress
 *
 * Zu diesen Einstellungen gehören:
 *
 * * MySQL-Zugangsdaten,
 * * Tabellenpräfix,
 * * Sicherheitsschlüssel
 * * und ABSPATH.
 *
 * Mehr Informationen zur wp-config.php gibt es auf der
 * {@link https://codex.wordpress.org/Editing_wp-config.php wp-config.php editieren}
 * Seite im Codex. Die Zugangsdaten für die MySQL-Datenbank
 * bekommst du von deinem Webhoster.
 *
 * Diese Datei wird zur Erstellung der wp-config.php verwendet.
 * Du musst aber dafür nicht das Installationsskript verwenden.
 * Stattdessen kannst du auch diese Datei als wp-config.php mit
 * deinen Zugangsdaten für die Datenbank abspeichern.
 *
 * @package WordPress
 */

// ** MySQL-Einstellungen ** //
/**   Diese Zugangsdaten bekommst du von deinem Webhoster. **/

/**
 * Ersetze datenbankname_hier_einfuegen
 * mit dem Namen der Datenbank, die du verwenden möchtest.
 */
define('DB_NAME', 'mhcp-website');

/**
 * Ersetze benutzername_hier_einfuegen
 * mit deinem MySQL-Datenbank-Benutzernamen.
 */
define('DB_USER', 'root');

/**
 * Ersetze passwort_hier_einfuegen mit deinem MySQL-Passwort.
 */
define('DB_PASSWORD', 'root');

/**
 * Ersetze localhost mit der MySQL-Serveradresse.
 */
define('DB_HOST', 'localhost');

/**
 * Der Datenbankzeichensatz, der beim Erstellen der
 * Datenbanktabellen verwendet werden soll
 */
define('DB_CHARSET', 'utf8mb4');

/**
 * Der Collate-Type sollte nicht geändert werden.
 */
define('DB_COLLATE', '');

define('WP_SITEURL', 'http://localhost:8888/mhcp-website/');
define('WP_HOME', 'http://localhost:8888/mhcp-website/');

/**#@+
 * Sicherheitsschlüssel
 *
 * Ändere jeden untenstehenden Platzhaltertext in eine beliebige,
 * möglichst einmalig genutzte Zeichenkette.
 * Auf der Seite {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * kannst du dir alle Schlüssel generieren lassen.
 * Du kannst die Schlüssel jederzeit wieder ändern, alle angemeldeten
 * Benutzer müssen sich danach erneut anmelden.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '/8W)?1REbyNalkGI`0FY,<rQs/%|bQ{`@?=rYcu$C2rh<(X)6/lLCKsz&N_{1t/w');
define('SECURE_AUTH_KEY',  'q/C_1D&&Mo>]c{M;{(sq~ZH|2CQ^bwzAdPDB%2K4B=97=pH+iV/;8*.h~L2S1Q(P');
define('LOGGED_IN_KEY',    'ixdoOJ@)Sw/kw>Q6AoiqEZMLj^ jxYz$D{-/}C_4#%!3X+7XCyz@hnM|w]J[%T1M');
define('NONCE_KEY',        '{?,tF27V7g8jc!DD9Aaa{m-<8*py[:`57C.ugCnQ2_OXTyZuQ9$U_6VIMt_$K8:B');
define('AUTH_SALT',        ' *#i!sPLq0&t3Dred$7e:>T^|(ClA$&QtC!!eOF<v-$[NG@jFc3ZFAZ_YlP1.dB>');
define('SECURE_AUTH_SALT', 'K?<E=|9kpLe8~* ?FQQGjt7jn7!YmuM-OwMknS*3gwdGQi;`6B9+h`l},B1.)wsY');
define('LOGGED_IN_SALT',   'KmGb~=@! Xn|UI=!_H3M(3T}K;oxS4 c@3/5<]G%~V>5`yDS[`UqDZM@zplp`=:r');
define('NONCE_SALT',       'E{^2upSxL|*>F*VCEd&GWUkE>~IHOVOhUIea|uAWGmCIECKva:uMQ%2iEOC(8Z9e');

/**#@-*/

/**
 * WordPress Datenbanktabellen-Präfix
 *
 * Wenn du verschiedene Präfixe benutzt, kannst du innerhalb einer Datenbank
 * verschiedene WordPress-Installationen betreiben.
 * Bitte verwende nur Zahlen, Buchstaben und Unterstriche!
 */
$table_prefix  = 'wp_';

/**
 * Für Entwickler: Der WordPress-Debug-Modus.
 *
 * Setze den Wert auf „true“, um bei der Entwicklung Warnungen und Fehler-Meldungen angezeigt zu bekommen.
 * Plugin- und Theme-Entwicklern wird nachdrücklich empfohlen, WP_DEBUG
 * in ihrer Entwicklungsumgebung zu verwenden.
 *
 * Besuche den Codex, um mehr Informationen über andere Konstanten zu finden,
 * die zum Debuggen genutzt werden können.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* Das war’s, Schluss mit dem Bearbeiten! Viel Spaß beim Bloggen. */
/* That's all, stop editing! Happy blogging. */

/** Der absolute Pfad zum WordPress-Verzeichnis. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Definiert WordPress-Variablen und fügt Dateien ein.  */
require_once(ABSPATH . 'wp-settings.php');
