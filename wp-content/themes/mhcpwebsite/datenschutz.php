<?php /* Template Name: Datenschutz */ ?>
<?php get_header(); the_post();?>

  <img src="<?php bloginfo('template_url')?>/img/background_muster.png" alt="" class="bg">

  <section id="datenschutz">
    <div class="container text-center header">
      <div class="row row1">
        <div class="col-12 text-center">
          <h2>Datenschutzerklärung</h2>
        </div>
      </div>
    </div>
    <div class="container">
      <div class="row row2">
        <div class="col-md-8">
          <div class="space"></div>
          <?php the_content(); ?>
          </div>
        <div class="col-md-4">
        </div>
      </div>
    </div>
  </section>

<?php get_footer(); ?>
