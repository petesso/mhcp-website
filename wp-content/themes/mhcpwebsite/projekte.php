<?php /* Template Name: Projekte */ ?>
<?php get_header(); ?>

  <img src="<?php bloginfo('template_url')?>/img/background_muster.png" alt="" class="bg">

  <section id="projekte">
    <div class="container text-center header">
      <div class="row row1">
        <div class="col-12 text-center">
          <h2>Ausgewählte Projekte der MHCP</h2>
        </div>
      </div>
    </div>
    <div class="container">
      <?php $query = new WP_Query(array('category_name' => 'Projekte')); ?>
      <?php if ($query->have_posts()) : while ($query->have_posts()) : $query->the_post(); ?>
        <div class="row row2">
          <div class="col-md-6">
            <h5>Projekt</h5>
            <p><?php echo get_field('projekt'); ?></p>
          </div>
          <div class="col-md-6">
            <h5>Unternehmen/Branche</h5>
            <p><?php echo get_field('unternehmen_branche'); ?></p>
          </div>
        </div>
      <?php endwhile; wp_reset_postdata(); endif; ?>

    </div>
  </section>

<?php get_footer(); ?>
