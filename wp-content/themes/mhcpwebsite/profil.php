<?php /* Template Name: Profil */ ?>
<?php get_header(); the_post();?>

  <img src="<?php bloginfo('template_url')?>/img/background_muster.png" alt="" class="bg">

  <section id="profil">
    <div class="container text-center header">
      <div class="row row1">
        <div class="col-12 text-center">
          <h2>Profil</h2>
        </div>
      </div>
    </div>
    <div class="container">
      <div class="row row2">
        <div class="col-lg-8">
          <?php the_content(); ?>
        </div>
        <div class="col-lg-4">
          <img src="<?php bloginfo('template_url')?>/img/profil_halstenberg.png" alt="profilbild_halstenberg">
        </div>
      </div>
    </div>
  </section>

<?php get_footer(); ?>
