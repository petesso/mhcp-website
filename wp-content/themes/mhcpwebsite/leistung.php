<?php /* Template Name: Leistung */ ?>
<?php get_header(); ?>

  <img src="<?php bloginfo('template_url')?>/img/background_muster.png" alt="" class="bg">

  <section id="leistung">
    <div class="container text-center header">
      <div class="row row1">
        <div class="col-12 text-center">
          <h2>Leistungsspektrum</h2>
          <h4>Bei allen Herausforderungen rund um das ERP-System proALPHA bieten wir Ihnen unsere Unterstützung:</h4>
        </div>
      </div>
    </div>
    <div class="container">
      <?php $query = new WP_Query(array('category_name' => 'Leistungen')); ?>
      <?php if ($query->have_posts()) : while ($query->have_posts()) : $query->the_post(); ?>
        <div class="row row2">
          <div class="col-md-12">
            <div class="text-leistung">
            <h5><?php echo get_field('leistung_header'); ?></h5>
            <p><?php echo get_field('leistung'); ?></p>
          </div>
        </div>
      </div>
      <?php endwhile; wp_reset_postdata(); endif; ?>
    </div>
    <div class="container">
      <div class="row row2">
        <div class="col-md-12">
          <div class="text-leistung">
          <h5>Einführung von piaX, der Offlinelösung der Mobileblox GmbH in den Bereichen:</h5>
          <h6>proALPHA CRM (bis Version 5.2: VIS), proALPHA Service, Bauobjektverwaltung</h6>
          <p>Als offizieller Beratungspartner der Mobileblox GmbH unterstützen wir sie bei der Einführung von piaX.</p>
          <a href="www.mobileblox.de"><img src="<?php bloginfo('template_url')?>/img/piax_logo.png" alt="piaxlogo"></a>
          </div>
        </div>
      </div>
    </div>
  </section>

<?php get_footer(); ?>
