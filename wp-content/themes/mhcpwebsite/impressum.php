<?php /* Template Name: Impressum */ ?>
<?php get_header(); the_post();?>

  <section id="impressum">
    <div class="container text-center header">
      <div class="row row1">
        <div class="col-12 text-center">
          <h2>Impressum</h2>
        </div>
      </div>
    </div>
    <div class="container">
      <div class="row row2">
        <div class="col-md-8">
          <?php the_content(); ?>
        </div>
        <div class="col-md-4">
        </div>
      </div>
    </div>
  </section>

<?php get_footer(); ?>
