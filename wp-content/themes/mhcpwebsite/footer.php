  <footer id="footer">
    <div class="container text-center">
      <div class="row row-top">
        <div class="col">
          <p>© MHCP 2018 - <a href="/mhcp-website/impressum">Impressum</a> - <a href="/mhcp-website/datenschutz">Datenschutzerklärung</a></p>
          <div class="space footer"></div>
        </div>
      </div>
    </div>
  </footer>

<?php wp_footer(); ?>
</body>
</html>
