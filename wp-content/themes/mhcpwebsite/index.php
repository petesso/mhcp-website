<?php /* Template Name: Home */ ?>
<?php get_header(); the_post();?>

  <img src="<?php bloginfo('template_url')?>/img/background_muster.png" alt="" class="bg">

  <section id="home">
    <div class="container">
      <div class="row oben">
        <div class="col-md-6 text-left">
          <?php the_content(); ?>
          <div class="space"></div>
          <div class="space"></div>
          <a href="/mhcp-website/contact" class="button">Kontakt</a>
        </div>
        <div class="col-md-6 bild">
          <div class="space_bild"></div>
        </div>
      </div>
    </div>

    <div class="container">
      <div class="row kacheln">
        <div class="col-md-12 col-lg-4 profilbild">
          <div class="img-container">
            <a href="/mhcp-website/profil">
              <img src="<?php bloginfo('template_url')?>/img/profil_bild.png" alt="leistungbild">
              <div class="text-container">
                <h2>Profil</h2>
              </div>
            </a>
          </div>
        </div>
        <div class="col-md-12 col-lg-4 leistungbild">
          <div class="img-container">
            <a href="/mhcp-website/leistung">
              <img src="<?php bloginfo('template_url')?>/img/leistung_bild.png" alt="leistungbild">
              <div class="text-container">
                <h2>Leistungsspektrum</h2>
              </div>
            </a>
          </div>
        </div>
        <div class="col-md-12 col-lg-4 projektebild">
          <div class="img-container">
            <a href="/mhcp-website/projekte">
              <img src="<?php bloginfo('template_url')?>/img/projekte_bild.png" alt="leistungbild">
              <div class="text-container">
                <h2>Projekte</h2>
              </div>
            </a>
          </div>
        </div>
      </div>
    </div>
  </section>

<?php get_footer(); ?>
