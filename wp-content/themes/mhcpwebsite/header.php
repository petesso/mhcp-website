<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <title>MHCP</title>

  <?php wp_head(); ?>
</head>

<body data-spy="scroll" data-target=".navbar" data-offset="50">

  <nav class="navbar navbar-expand-md navbar-light" role="navigation">
    <div class="container">
      <a class="navbar-brand" href="index.html">
        <img alt="mhcp logo" src="<?php bloginfo('template_url')?>/img/mhcp_logo.svg">
      </a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
        <i class="fas fa-bars"></i>
      </button>
      <div class="collapse navbar-collapse" id="navbarNav">
        <ul class="nav navbar-nav ml-auto">
          <li class="nav-item">
            <a class="nav-link page-scroll" href="/mhcp-website/">HOME</a>
          </li>
          <li class="nav-item">
            <a class="nav-link page-scroll" href="/mhcp-website/profil">PROFIL</a>
          </li>
          <li class="nav-item">
            <a class="nav-link page-scroll" href="/mhcp-website/leistung">LEISTUNGEN</a>
          </li>
          <li class="nav-item">
            <a class="nav-link page-scroll" href="/mhcp-website/projekte">PROJEKTE</a>
          </li>
          <li class="nav-item">
            <a class="nav-link page-scroll" href="/mhcp-website/contact">KONTAKT</a>
          </li>
        </ul>
      </div>
    </div>
  </nav>
