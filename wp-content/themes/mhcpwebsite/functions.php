<?php

function add_theme_scripts() {
  wp_enqueue_style('font', 'https://fonts.googleapis.com/css?family=Lato:300,400,700,900');

  wp_enqueue_style('bootstrap', get_template_directory_uri() . '/library/bootstrap/css/bootstrap.min.css');

  wp_enqueue_style('nav', get_template_directory_uri() . '/css/nav.css');
  wp_enqueue_style('home', get_template_directory_uri() . '/css/home.css');
  wp_enqueue_style('footer', get_template_directory_uri() . '/css/footer.css');
  wp_enqueue_style('contact', get_template_directory_uri() . '/css/contact.css');
  wp_enqueue_style('projekte', get_template_directory_uri() . '/css/projekte.css');
  wp_enqueue_style('leistung', get_template_directory_uri() . '/css/leistung.css');
  wp_enqueue_style('profil', get_template_directory_uri() . '/css/profil.css');
  wp_enqueue_style('partner', get_template_directory_uri() . '/css/partner.css');
  wp_enqueue_style('impressum', get_template_directory_uri() . '/css/impressum.css');
  wp_enqueue_style('datenschutz', get_template_directory_uri() . '/css/datenschutz.css');
  wp_enqueue_style('main', get_template_directory_uri() . '/css/main.css');

  wp_deregister_script('jquery');
  wp_enqueue_script('jquery', get_template_directory_uri() . '/library/jquery/jquery.min.js', array(), null, true);
  wp_enqueue_script('fontawesome', get_template_directory_uri() . '/library/fontawesome/fontawesome-all.min.js', array('jquery'), null, true);
  wp_enqueue_script('bootstrap', get_template_directory_uri() . '/library/bootstrap/js/bootstrap.min.js', array('jquery'), null, true);
  wp_enqueue_script('contact', 'https://mandaris.s3.amazonaws.com/contact.js', array('jquery'), null, true);
  wp_enqueue_script('md', get_template_directory_uri() . '/js/md.js', array('contact'), null, true);
}

add_action('wp_enqueue_scripts', 'add_theme_scripts');

?>
