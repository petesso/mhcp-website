<?php /* Template Name: Contact */ ?>
<?php get_header(); the_post();?>

  <section id="contact">
    <div class="container text-center">
      <div class="row row1">
        <div class="col-12 text-center">
          <div class="title-line">
            <?php the_content(); ?>
            <div class="space"></div>
          </div>
        </div>
        <div class="col-md-3"></div>
        <div class="col-md-6">
          <div class="row">
            <div class="col-sm-6 form-group">
              <input class="form-control" id="name" name="name" placeholder="Name" type="text" required>
            </div>
            <div class="col-sm-6 form-group">
              <input class="form-control" id="email" name="email" placeholder="Email" type="email" required>
            </div>
          </div>
          <textarea class="form-control" id="comments" name="comments" placeholder="Nachricht" rows="5"></textarea>
          <br>
          <div class="row">
            <div class="col">
              <div class="space"></div>
              <a href="#contact" id="send" class="button">Senden</a>
            </div>
          </div>
          <br>
        </div>
        <div class="col-md-3"></div>
      </div>
    </div>
  </section>

  <section>
    <div class="container">
      <div class="row">
        <div class="col">
          <div class="space"></div>
        </div>
      </div>
    </div>
  </section>

<?php get_footer(); ?>
